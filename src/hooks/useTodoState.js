import { useState } from 'react';
import useLocalStorageState from './useLocalStorageState';
import uuid from 'uuid/v4';

function useTodoState(initialTodos = []) {
  const [todos, setTodos] = useLocalStorageState('todos', initialTodos || []);

  return {
    todos,
    addTodo: newTodoText => {
      setTodos([...todos, { id: uuid(), task: newTodoText, completed: false }]);
    },
    removeTodo: todoId => {
      const updatedTodos = todos.filter(todo => todo.id !== todoId);
      setTodos(updatedTodos);
    },
    toggleTodo: todoId => {
      console.log('inside toggle todo');
      const updatedTodos = todos.map(todo =>
        todo.id === todoId ? { ...todo, completed: !todo.completed } : todo
      );
      setTodos(updatedTodos);
    },
    editTodo: (todoId, newTask) => {
      const updatedTodos = todos.map(todo => {
        return todo.id === todoId ? { ...todo, task: newTask } : todo;
      });
      setTodos(updatedTodos);
    }
  };
}
export default useTodoState;
