import React from 'react';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';

import Todo from './Todo';

export default function(props) {
  if (props.todos.length > 0) {
    return (
      <Paper>
        <List>
          {props.todos.map((todo, i) => {
            return (
              <>
                <Todo
                  toggleTodo={props.toggleTodo}
                  removeTodo={props.removeTodo}
                  editTodo={props.editTodo}
                  id={todo.id}
                  task={todo.task}
                  key={todo.id}
                  completed={todo.completed}
                />
                {props.todos.length - 1 === i ? null : <Divider />}
              </>
            );
          })}
        </List>
      </Paper>
    );
  }

  return null;
}
