import React, { useState, useEffect } from 'react';
import uuid from 'uuid/v4';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';

import TodosList from './TodosList';
import TodoForm from './TodoForm';
import useTodoState from './hooks/useTodoState';

function TodoApp() {
  const initialTodos = [];

  const { todos, addTodo, removeTodo, toggleTodo, editTodo } = useTodoState(
    initialTodos
  );

  return (
    <Paper
      style={{
        paddig: 0,
        margin: '1rem',
        height: '100vh',
        backgroudColor: '#f1f1f1'
      }}
      elevation={3}
    >
      <AppBar color="primary" position="static" style={{ height: '64px' }}>
        <Toolbar>
          <Typography color="inherit">Todos With Hooks</Typography>
        </Toolbar>
      </AppBar>
      <Grid container justify="center" style={{ marginTop: '1rem' }}>
        <Grid item xs={11} md={8} lg={4}>
          <TodoForm addTodo={addTodo} />
          <TodosList
            todos={todos}
            toggleTodo={toggleTodo}
            removeTodo={removeTodo}
            editTodo={editTodo}
          />
        </Grid>
      </Grid>
    </Paper>
  );
}

export default TodoApp;
